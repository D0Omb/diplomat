﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public List<int> scores = new List<int>();

    public GameData(List<int> scores) {
        this.scores = scores;
    }
}
