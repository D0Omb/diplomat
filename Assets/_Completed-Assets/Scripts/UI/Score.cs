﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;

public class Score : MonoBehaviour
{
    private int score = 0;
    public Text scoreScr;

    public void Increase() {
        score++;
        scoreScr.text = "Score: " + score;
    }

    public void Save() {
        List<int> scores = LoadFile();
        if (scores == null) {
            scores = new List<int>();
        }

        string destination = Application.persistentDataPath + "/save.dat";
        FileStream file;

        if (File.Exists(destination)) 
            file = File.OpenWrite(destination);
        else 
            file = File.Create(destination);

        scores.Add(score);

        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(file, new GameData(scores));
        file.Close();
    }

    public List<int> LoadFile() {
        string destination = Application.persistentDataPath + "/save.dat";
        FileStream file;

        if (File.Exists(destination)) {
            file = File.OpenRead(destination);
        } else {
            Debug.LogError("File not found");
            return null;
        }

        BinaryFormatter bf = new BinaryFormatter();
        GameData data;
        try {
            data = (GameData) bf.Deserialize(file);
        } catch (Exception e) {
            Debug.Log(e);
            return null;
        } finally {
            file.Close();
        }

        return data.scores;
    }
}
