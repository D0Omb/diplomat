﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankSpawner : MonoBehaviour
{
    public GameObject enemyTank;
    private float timeToSpawn = 2;
    public float initTimeToSpawn = 10;

    public GameObject spawner1;
    public GameObject spawner2;
    public GameObject spawner3;
    public GameObject spawner4;
    public GameObject spawner5;

    public void Update() {
        timeToSpawn -= Time.deltaTime;
        if (timeToSpawn < 0) {
            timeToSpawn = initTimeToSpawn;
            Spawn();
        }
    }

    private void Spawn() {
        int rn = Random.Range(0, 5);
        GameObject obj;
        switch(rn) {
            case 0:
                obj = Object.Instantiate(enemyTank, spawner1.transform.position, Quaternion.identity);
                obj.SetActive(true);
                break;
            case 1:
                obj = Object.Instantiate(enemyTank, spawner2.transform.position, Quaternion.identity);
                obj.SetActive(true);
                break;
            case 2:
                obj = Object.Instantiate(enemyTank, spawner3.transform.position, Quaternion.identity);
                obj.SetActive(true);
                break;
            case 3:
                obj = Object.Instantiate(enemyTank, spawner4.transform.position, Quaternion.identity);
                obj.SetActive(true);
                break;
            case 4:
                obj = Object.Instantiate(enemyTank, spawner5.transform.position, Quaternion.identity);
                obj.SetActive(true);
                break;
        }
    }
        
}