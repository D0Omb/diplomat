﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScreen : MonoBehaviour
{
    public Score score;

    public void Retry()
    {
        score.Save();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    public void Quit()
    {
        score.Save();
        SceneManager.UnloadScene("Пустыня");
        SceneManager.LoadScene("Menu");
    }
}
